FROM node:10.4-alpine AS base

FROM base AS build-env
WORKDIR /src
COPY package*.json ./
RUN npm install
COPY . .

FROM build-env AS build
RUN npm run build

FROM build-env AS unit_test
RUN npm run test:unit

FROM cypress/base AS e2e_test
WORKDIR /src
COPY --from=build-env /src/ ./
RUN npm run test:e2e -- --headless

FROM base AS dist
WORKDIR /app
COPY --from=build /dist .