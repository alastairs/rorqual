import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

interface RorqualState {
  processingQueue: File[];
}

export const mutations = {
  pushFile(state: RorqualState, file: File) {
    state.processingQueue.push(file);
  },

  popFile(state: RorqualState): File {
    return state.processingQueue.splice(0, 1)[0];
  },
};

export const actions = {

};

export default new Vuex.Store({
  state: {
    processingQueue: [],
  } as RorqualState,
  mutations,
  actions,
});
