// tslint:disable:no-unused-expression
import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import LogViewer from '@/components/LogViewer.vue';

describe('Log Viewer', () => {
  const wrapper = shallowMount(LogViewer);

  it('displays a file dropzone', () => {
    expect(wrapper.find('#dropzone')).not.to.be.empty;
    expect(wrapper.vm.$data.dropzoneOptions).to.exist;
  });

  describe('Dropzone', () => {
    const dropzone = wrapper.vm.$data.dropzoneOptions;

    it('is clickable', () => {
      expect(dropzone.clickable).to.be.true;
    });

    it('supports processing multiple files', () => {
      expect(dropzone.uploadMultiple).to.be.true;
    });

    it('prompts the user to add a file', () => {
      expect(dropzone.dictDefaultMessage).to.contain('Add log file');
    });

    it('allows the user to change the files to be processed', () => {
      expect(dropzone.addRemoveLinks).to.be.true;
    });

    it('forbids multiple log files from being processed', () => {
      expect(dropzone.maxFiles).to.equal(1);
    });
  });
});
